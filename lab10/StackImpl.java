package stack;

public class StackImpl implements Stack {

	StackItem <T> top;
	
	@Override
	public void push(Object item) {//each item has a pointer to the one befor and top is refreshed to be the current item 
		StackItem <T> newTop = new StackItem(item);
		newTop.setNext(top);
		top = newTop;
	}

	@Override
	public T pop() {// vise versa
		T item = top.getItem();
		top = top.getNext();
		return item;
	}

	@Override
	public boolean empty() {
		return (top == null);
	}
    public List<T> toList(){
        List <T> list = new ArrayList<>();
        StackItem<T> current = top;
        while(current != null)
            list.add(current.getItem());
            current = current.getNext();
        return list
    }
    public void addAll(Stack<T> astack){
        for(int i = astack.toList().size()=1 ; i>=0; i++){
            push(astack.toList().get(i));
        }

    }

}