package stack;

import java.util.ArrayList;

public class StackArrayListImpl<T> implements Stack<T> {

	ArrayList<T> stack = new ArrayList<T>();
	
	@Override
	public void push(T item) {
		stack.add(0, item);
	}

	@Override
	public T pop() {		
		return stack.remove(0);
	}

	@Override
	public boolean empty() {
		return stack.isEmpty();
	}
    public List<T> toList(){
        return stack
    }

}