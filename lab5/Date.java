
public class Date {
	
		int day;
		int month; 
		int year;

		int[] maxDays = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		
		public Date(int day, int month, int year) {
			this.day = day;
			this.month = month-1; //since idxs start up with zero
			this.year = year;
		}
		
		public void incrementDay() {
			int newDay = day+1;
			if (newDay > maxDays[month]){
				day =1;
				incrementMonth();
			}else if(month == 1 && newDay ==29 && !Leap()){
				day =1;
				incrementMonth();	//icrementing once getting to day 29 in case of leap years		
			}else{
				day = newDay;
			}
			
		}
		public void decrementDay() {
			int newDay = day - 1;
			if (newDay == 0){
				day = 31;
				decrementMonth(); 
			}else{
				day = newDay;
			}
			
		}
		public void incrementDay(int i) {
			while (i > 0){
				incrementDay();//looping through a no. of days i inncrementing up to it and so on ivrementing the month if needed
				i--;
			}
			
		}		
		public void decrementDay(int i) {
			while (i > 0){
				decrementDay();//same as incrementing 
				i--;
			}
			
		}
		public void incrementMonth(int i) {
			int newMonth = (month + i) % 12;
			int yearChange = 0;
			//handling the addition of a no. that leads to a grater month no. than 12
			
			if (newMonth < 0) {//in case of negative value
				newMonth += 12;
				yearChange = -1;
			}		
			yearChange += (month + i) / 12;//handing the year incrementation through the additional of no. that leads to a grater month no. than 12
			month = newMonth;
			year += yearChange;
			if (day > maxDays[month]){
				day = maxDays[month];
				if(month ==1 && day ==29 && !Leap()){
					day =28;
				}
			}
		}	
		public void incrementMonth() {
			incrementMonth(1);
			
		}

		public void decrementMonth(int i) {
			incrementMonth(-i);
			
		}
		
		public void decrementMonth() {
			incrementMonth(-1);
			
		}
		
		public void incrementYear(int i) {
			year+=i;
			if (month ==1 && day ==29 && !Leap()){
				day = 28;
			}
			
		}

		public void incrementYear() {
			incrementYear(1);
			
		}

		public void decrementYear() {
			incrementYear(-1);		
		}


		public void decrementYear(int i) {
			incrementYear(-i);
			
		}
		
		public String toString(){
			
		//writing the date in a form like 1996-04-21 for ex addin 1 to month since it's 1 less than the right value though addin an unvaluable zero if a value gets to be less than 10
			return year + "-"+(month + 1 < 10 ? "0" : "") + (month + 1) + "-" +(day < 10 ? "0" : "") + day; 
		}
		
		public boolean Leap(){
			return year % 4 == 0 ? true : false;
		}		
		public boolean isBefore(Date Date2) {
			
		//putting the date in a string form
			int a = Integer.parseInt(toString().replaceAll("-", "")) ;

			int b = Integer.parseInt(Date2.toString().replaceAll("-", ""));

			return a < b;
		}		
		public boolean isAfter(Date Date2) {
			int a = Integer.parseInt(toString().replaceAll("-", "")) ;
			int b = Integer.parseInt(Date2.toString().replaceAll("-", ""));
			return a > b;
		}	
		
		
		//??????
		public int dayDifference(Date Date2) {
			int diff = 0;
			if (isBefore(Date2)){
				Date date = new Date(day,month+1,year); 
				while (date.isBefore(Date2)){
					date.incrementDay();
					diff++;
				}
			}else if(isAfter(Date2)){
			    Date date = new Date(day,month+1,year); 
				while (date.isAfter(Date2)){
					date.decrementDay();
					diff++;
				}
				
			}
			return diff;
		}		
}
