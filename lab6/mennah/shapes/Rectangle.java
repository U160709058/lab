package mennah.shapes;

public class Rectangle {
	int sideA;
	int sideB;
	public Rectangle(int sideA,int sideB){
		this.sideA=sideA;
		this.sideB=sideB;
	}
	public double Area(){
		return sideA*sideB;	
	}

}
