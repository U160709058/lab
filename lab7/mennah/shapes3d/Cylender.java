package mennah.shapes3d;
import mennah.shapes.*; //as  super class
public class Cylender {
    private int height;
    public Cylender(int radius,int height){
    super(radius);//the costructor of the super class with super can be called
    //or this.radius = radius needs constructor wit no paramenters in the original circle class to get no errors 
    this.height = height;
    }
	private static double getHeight() {
	
	return height;
}

    public double area(){
    return 2*3.14*getRadius()*getRadius()//super.area()so it doesn't become a recursive + 2*3.14*getRadius()*height;
    }

    public double volume(){
    return 3.14*radius*radius*height;
    }

}
