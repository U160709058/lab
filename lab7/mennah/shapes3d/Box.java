package mennah.shapes3d;
import mennah.shapes.Rectangle;
public class Box {
    int sideC;
    public Box(int sideA, int sideB,sideC){
    super(sideA);
    super(sideB);
    this.sideC = sideC;
    }
	public int area(){
		return 2 * super.area() + 2 * sideA * sideC + 2 * sideB * sideC;

	}

    public double volume(){
        return area*sideC;
    }

}
