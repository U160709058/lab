package mennah.shapes;

public class Rectangle {
    protected int sideA;//sub classes can access them
    protected int sideB;
    public Rectangle(int sideA, int sideB){
        this.sideA = sideA;
        this.sideB = sideB;
	}
	public double area(){
	return sideA*sideB;
	}

}
